
<!-- README.md is generated from README.Rmd. Please edit that file -->

# bbmixpaper

The goal of bbmixpaper is to provide the code for the analysis of the
bbmix paper. The pipeline describing the analysis done in the paper can
be found at
[bbmix\_pipeline](https://gitlab.com/evigorito/bbmix_pipeline)

## System requirements

    R versions >= 3.4.0.
    GNU make >= 3.82

## Instal bbmixpaper from [GitLab](https://gitlab.com):

``` r
## Within R:
install.packages("devtools") # if you don't already have the package
library(devtools)
devtools::install_git(url = "https://gitlab.com/evigorito/bbmixpaper.git") 
```
